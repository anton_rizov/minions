#include <MsgBoxConstants.au3>

Local $startPage = InputBox("Start Page", "Start Page: ")
Local $endPage = InputBox("End Page", "End Page: ")
Local $hwnd = WinActivate("[CLASS:AcrobatSDIWindow]")
If $hwnd == 0 Then
   MsgBox($MB_OK, "Error","Cannot find Adobe Reader")
EndIf
For $page = $startPage To $endPage Step 2
    Send("^p")
    WinWaitActive("Print", "Pa&ges")
    Send("!g")
    Send("{TAB}")
    Send($page & "-" & ($page + 1))
    Send("{ENTER}")
    Sleep(60000)
Next
