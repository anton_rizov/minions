(defun digits (x)
  (loop with reminder
        do (multiple-value-setq (x reminder)
             (truncate x 10))
        collect reminder
        until (zerop x)))

(defun happyp (x memory)
  (labels ((square (x)
             (expt x 2))
           (frob (n)
             (reduce #'+ (mapcar #'square (digits n)))))
    (if (= x 1)
        t
        (multiple-value-bind (value present-p)
            (gethash x memory)
          (if present-p
              value
              (progn
                ;; mark the number as unhappy.  If it is in fact happy
                ;; we'll update it again.  Keep track of it so cycles
                ;; get detected.
                (setf (gethash x memory) nil)
                (when (happyp (frob x) memory)
                  (setf (gethash x memory) t))))))))


(defun happy-numbers (n)
  (loop with memory = (make-hash-table)
        for i from 1 to n
        if (happyp i memory)
        collect i))

;; (defun main (n)
;;   (let ((memory (make-hash-table)))
;;     (loop for i from 1 below n
;; 	  do (format T "~& ~A is ~Ahappy.~%"
;; 		     i
;; 		     (if (happyp i memory) "" "not ")))))

;;; keeping only the current cycle
;; (defun happyp (x memory)
;;   (labels ((square (base)
;; 	     (expt base 2))
;; 	   (frob (n)
;; 	     (reduce #'+ (mapcar #'square (digits n)))))
;;     (if (= x 1)
;; 	t
;; 	(unless (member x memory)
;; 	  (happyp (frob x) (cons x memory))))))

;; (defun main (n)
;;   (loop for i from 1 to n
;; 	if (happyp i nil)
;; 	collect i))
