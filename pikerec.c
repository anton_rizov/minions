#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum RE_Type
{
	CHAR,
	STAR,
	LIT,
	END,
	EXIT
};

struct RE
{
	enum RE_Type type;
	char c;
	char *ccl;
	bool neg;
};

size_t count_re(const char *rx)
{
	size_t count = 1 /* EXIT */;
	char c;

	if (!rx)
		return 0;

	if (*rx != '^')
		++count;
	else
		++rx;

	while (c = *rx++) {
		if (c == '+' || c == '*')
			continue;

		++count;

		if (c == '\\' && rx)
			++rx;
	}

	return count;
}

struct RE *
compile(const char *regex, struct RE *p)
{
	if (*regex == '^')
		++regex;
	else if (*regex) {
		p->c = '.';
		p->type = STAR;
		++p;
	}

	while (*regex) {
		char c;
		p->c = *regex++;
		p->type = CHAR;

		if (p->c == '\\' && *regex) {
			p->c = *regex++;
			p->type = LIT;
		} else if (p->c == '$' && !*regex)
			p->type = END;
		else
			switch (*regex) {
			case '+':
				c = p->c;
				(++p)->c = c;
			case '*':
				p->type = STAR;
				++regex;
			}
		p++;

	}
	return p;
}

struct RE *
compile2(const char *regex)
{
	struct RE *re, *p;
	int count;

	count = count_re(regex);

	p = re = (struct RE *) malloc(count * sizeof(struct RE));
	if (!re)
		exit(1);

	if (*regex == '^')
		++regex;
	else if (*regex) {
		p->c = '.';
		p->type = STAR;
		++p;
	}

	while (*regex) {
		char c;
		p->c = *regex++;
		p->type = CHAR;

		if (p->c == '\\' && *regex) {
			p->c = *regex++;
			p->type = LIT;
		} else if (p->c == '$' && !*regex)
			p->type = END;
		else
			switch (*regex) {
			case '+':
				c = p->c;
				(++p)->c = c;
			case '*':
				p->type = STAR;
				++regex;
			}
		p++;

	}

	p->type = EXIT;

	return re;
}



void print_re(const struct RE *re)
{
	if (re->type == CHAR)
		printf("%c\n", re->c);
	else if (re->type == STAR)
		printf("%c*\n", re->c);
	else if (re->type == END)
		printf("END\n");
	else if (re->type == EXIT)
		printf("exit");
}

void print_program(const struct RE *prog, const struct RE *end)
{
	while (prog < end)
		print_re(prog++);
}

void print_prog(const struct RE *prog)
{
	const struct RE *p;
	for (p = prog; p->type != EXIT; ++p)
		print_re(p);
}

bool matchchar(char c, const char *text)
{
	return *text && (*text == c || c == '.');
}

bool matchlit(char c, const char *text)
{
	return *text && *text == c;
}

bool match(const struct RE *re, const struct RE *end, const char *text);

bool matchstar(char c, const struct RE *prog, const struct RE *end, const char *text)
{
	const char *t = text;

	while (matchchar(c, t))
		++t;

	do {
		if (match(prog, end, t))
		    return true;
	} while (t-- > text);

	return false;
}

bool match(const struct RE *re, const struct RE *end, const char *text)
{
	while (re < end) {
		if (re->type == CHAR && !matchchar(re->c, text))
			return false;
		if (re->type == LIT && !matchlit(re->c, text))
			return false;
		if (re->type == END && *text)
			return false;
		if (re->type == STAR)
			return matchstar(re->c, re + 1, end, text);
		if (!*text)
			return false;
		++text;
		++re;
	}
	return true;
}

bool match2(const struct RE *re, const char *text);

bool matchstar2(char c, const struct RE *prog, const char *text)
{
	const char *t = text;

	while (matchchar(c, t))
		++t;

	do {
		if (match2(prog, t))
		    return true;
	} while (t-- > text);

	return false;
}

bool match2(const struct RE *re, const char *text)
{
	const struct RE *p;

	for (p = re; p->type != EXIT; ++p,++text) {
		if (p->type == CHAR && !matchchar(p->c, text))
			return false;
		if (p->type == LIT && !matchlit(p->c, text))
			return false;
		if (p->type == END && *text)
			return false;
		if (p->type == STAR)
			return matchstar2(p->c, p + 1, text);
		if (!*text)
			return false;
	}
	return true;
}


void t(const struct RE *re, const struct RE *end, const char *text)
{
	printf("%s: %s\n", match(re, end, text) ? "yes" : " no", text);
}

void t2(const struct RE *re, const char *text)
{
	printf("%s: %s\n", match2(re, text) ? "yes" : " no", text);
}

void main()
{
	struct RE program[255];
	struct RE *end = compile("a*b", program);

	struct RE *p;

	printf("%lu\n", count_re("a*b"));
	print_program(program, end);
	t(program, end, "b");
	t(program, end, "a");
	t(program, end, "xa");
	t(program, end, "ab");
	t(program, end, "aaaab$");


        end = compile("\\.", program);
	t(program, end, "x");

	printf("----\n");

	p = compile2("a*b^$");
	print_prog(p);
	t2(p, "aab");
	t2(p, "b");
	t2(p, "xy");
	free(p);
}
