#/bin/sh

percent=${1:-60}

bat=/sys/class/power_supply/BAT0
full=`cat $bat/charge_full_design`
max=$(($full * $percent / 100))
while : ;do
    current=`cat $bat/charge_now`
    if (($current >= $max)); then
	    break
    fi
    sleep 60
done
