#/bin/sh

bat=/sys/class/power_supply/BAT0
full=`cat $bat/charge_full_design`
percent=0
while((percent < 50)); do
    current=`cat $bat/charge_now`
    percent=$(($current * 100 / $full))
    sleep 60
done
beep