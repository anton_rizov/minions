require 'cvs/parser.rb'

class FindByComment < CVS::Visitor
  def initialize message_pattern
    @message_pattern = Regexp.new(message_pattern)
    @found = false
  end
  
  def rcsfile(newRcsFile)
     @rcsFile = newRcsFile
  end

  def delta_rlog(rev, locked_by, date, author, state, add, del, branches, message)
    return if @found
    if message =~ @message_pattern
      puts @rcsFile
      puts "new revision: #{rev}"
      @found = true
    end
 end
end


log = CVS::Parser::Log.new($stdin)
log.parse(FindByComment.new(ARGV[0])) while true

