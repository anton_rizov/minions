(defun anagram (word chars)
  (if (null chars)
      (format t "~%~A" word)
      (loop for char in chars
            do (anagram (cons char word) (remove char chars :count 1)))))

(defconstant vowels '(a e i o u y))

(defun vowelp (c)
  (member c vowels))

(defun consonantp (c)
  (not (vowelp c)))

(defun anagram2 (nc word chars)
  (if (null chars)
      (format t "~%~A" word)
      (when (< nc 4)
        (loop for char in chars
              do (anagram2 (if (vowelp char) 0 (1+ nc))
                           (cons char word)
                           (remove char chars :count 1))))))


