#!/bin/sh

search_term=$(perl -MURI::Escape -e 'print uri_escape(join(" ", @ARGV));' "$@")
firefox -new-tab "http://www.google.com/search?hl=en&q=$search_term"
