;Author: Autohotkey forum user RHCP
;http://www.autohotkey.com/board/topic/103174-dual-function-control-key/
$~*Ctrl::
if !state
    state :=  (GetKeyState("Shift", "P") ||  GetKeyState("Alt", "P") || GetKeyState("LWin", "P") || GetKeyState("RWin", "P")) || (GetKeyState("LButton", "P")) || (GetKeyState("RButton", "P")) || (GetKeyState("MButton", "P")) 
return

$~ctrl up::
if instr(A_PriorKey, "control") && !state
    send {esc}
state := 0
return
