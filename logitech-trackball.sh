#!/bin/sh

device="Logitech USB Trackball"

xinput set-button-map "$device" 3 2 1 4 5 6 7 9 8
xinput set-prop "$device" 290 1

gsettings set org.gnome.settings-daemon.peripherals.mouse middle-button-enabled true
