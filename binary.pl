sub binary {
  my ($n) = @_;
  return $n if $n == 0 or $n == 1;

  my $k = int($n/2);
  my $b = $n % 2;

  return binary($k) . $b;
}

sub binary2 {
  my ($n) = @_;
  my $r = "";
  do {
	$r .= $n % 2;
	$n = int($n/2);	
  } while ($n);
  return reverse $r;
}

while (<>) {
  print binary($_) . "\n";
  print binary2($_) . "\n";
}

# lib
# sprintf("%b", val)
