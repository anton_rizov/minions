#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char digits[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

int poweroftwo(unsigned n)
{
        int      i = 1;
        unsigned x = 2;
        for (; x<=n && x != 0; ++i, x<<=1)
                if (x == n)
                        return i;
        return 0;
}

char *strrev(char *str)
{
        size_t len = strlen(str);
        char *h;
        char *t;
        char c;

        if (len > 1) {
                h = str;
                t = str + len - 1;
                while (h < t) {
                        c = *h;
                        *h = *t;
                        *t = c;
                        h++;
                        t--;
                }
        }
        return str;
}

char *toa(unsigned int value, char *string, int radix)
{
        char         *cp;
        unsigned int p;
        unsigned int mask;
        div_t        d;

        cp = string;

        if (value == 0)
                *cp++ = '0';
        else if (p = poweroftwo(radix)) {
                mask = (unsigned) ~(-1 << p);
                while (value) {
                        *cp++ = digits[value & mask];
                        value >>= p;
                }
        }
        else
                while (value) {
                        d = div(value, radix);
                        *cp++ = digits[d.rem];
                        value = d.quot;
                }


        *cp=0;

        return strrev(string);
}

int main()
{
        int pow;
        char *s = (char*)malloc(20);

        for (pow=2; pow <= 8; pow++)
                printf("%d %s\n", pow, toa(56, s, pow));

        free(s);
}
