/**
 * Open Windows Explorer's Tools, Folder Options, File types tab and
 * locate the HTTP protocol, labeled " URL:HyperText Transfer
 * Protocol'. Select it, then the Advanced button. Select 'open' and
 * Edit. In the 'Application used to perform action:' enter 
 *   wscript.exe _PATH_\dispatch.js "%1"
 * Remove the check from the Use DDE box and exit back to the File types
 * dialog. Repeat for HTTPS and FTP protocols.  
 */
var args=WScript.Arguments
var url=args.item(0)

var shell=WScript.CreateObject("WScript.Shell")
if (/projectserver/.test(url) ||  /[\/\\]ps[\/\\]/.test(url)) {
  shell.run('"c:\\Program Files\\Internet Explorer\\iexplore.exe" "' + url + '"')
} else {
  shell.run('"c:\\Program Files\\Mozilla Firefox\\firefox.exe" -url "' + url + '"')
}
shell=null