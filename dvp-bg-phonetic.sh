#!/bin/sh


#PREFIX=./fc14
PREFIX=

bak=`date +%Y-%m-%d`

mkdir $bak

cp --parents $PREFIX/usr/share/X11/xkb/symbols/bg 
             $PREFIX/usr/share/X11/xkb/rules/evdev.xml
             $PREFIX/usr/share/X11/xkb/rules/base.xml 
   $bak

ed  $PREFIX/usr/share/X11/xkb/rules/evdev.xml <<EOF
/<name>bas_phonetic</
/variantList>/a
      <variantList>
        <variant>
          <configItem>
            <name>bpd_phonetic</name>
            <description>Programmer Dvorak phonetic</description>
          </configItem>
        </variant>
      </variantList>
.
w
EOF

ed  $PREFIX/usr/share/X11/xkb/rules/base.xml <<EOF
/<name>bas_phonetic</
/variantList>/a
      <variantList>
        <variant>
          <configItem>
            <name>bpd_phonetic</name>
            <description>Programmer Dvorak phonetic</description>
          </configItem>
        </variant>
      </variantList>
.
w
EOF

ed $PREFIX/usr/share/X11/xkb/symbols/bg <<EOF
$ a
// BG phonetic based on Programmer Dvorak, by Roland Kaufmann <rlndkfmn at gmail dot com>
// License: BSD, available at <http://www.kaufmann.no/roland/dvorak/license.html>
// Main features: Numbers are in shift position (like French), symbols have been
// placed in locations that give good hand-alternation and finger rolling with
// symbols that usually follows, accented characters are possible for I18N.
partial alphanumeric_keys
xkb_symbols "bpd_phonetic" {
    include "us(dvorak)"

    name[Group1] = "Bulgarian - Programmer Dvorak";

    //             Unmodified       Shift           AltGr            Shift+AltGr
    // symbols row, left side
    key <TLDE> { [ question,          asciitilde,     dead_tilde                  ] };
    key <AE01> { [ slash,       percent                                     ] };
    key <AE02> { [ bracketleft,     7,              currency                    ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE03> { [ braceleft,       5,              cent                        ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE04> { [ braceright,      3,              yen                         ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE05> { [ parenleft,       1,              EuroSign                    ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE06> { [ equal,           9,              sterling                    ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };

    // symbols row, right side
    key <AE07> { [ asterisk,        0                                           ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE08> { [ parenright,      2,              onehalf                     ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE09> { [ plus,            4                                           ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE10> { [ bracketright,    6                                           ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE11> { [ exclam,          8,              exclamdown                  ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <AE12> { [ numbersign,      grave,          dead_grave                  ] };
    key <BKSP> { [ BackSpace                                                    ] };
  
    // upper row, left side
    //key <AD01> { [ semicolon,       colon,          dead_diaeresis              ] };
    key <AD01> {   [ Cyrillic_che,      Cyrillic_CHE        ]    };
    key <AD02> { [ comma,           less,           guillemotleft               ] };
    key <AD03> { [ period,          greater,        guillemotright              ] };
    key <AD04> { [ Cyrillic_pe,	    Cyrillic_PE,     paragraph,       section    ] };
    key <AD05> { [ Cyrillic_hardsign, Cyrillic_HARDSIGN   ]    };

    // upper row, right side
    key <AD06> { [ Cyrillic_ef,     Cyrillic_EF	  ]	};
    key <AD07> { [ Cyrillic_ghe,    Cyrillic_GHE         ]    };
    key <AD08> { [ Cyrillic_tse,    Cyrillic_TSE,   ccedilla,        Ccedilla   ] };
    key <AD09> { [ Cyrillic_er,	    Cyrillic_ER,    registered,      trademark  ] };
    key <AD10> { [ Cyrillic_el,	    Cyrillic_EL] };
    //key <AD11> { [ slash,           question,       questiondown                ] };
    key <AD11> { [ Cyrillic_sha,      Cyrillic_SHA                ] };
    key <AD12> {   [ Cyrillic_shcha,    Cyrillic_SHCHA      ]    };
    //{ [ at,              asciicircum,    dead_circumflex, dead_caron ] };

    // home row, left side
    key <AC01> { [ Cyrillic_a,      Cyrillic_A,     aring,           Aring      ] };
    key <AC02> { [ Cyrillic_o,	    Cyrillic_O,	    oslash,          Ooblique   ] };
    key <AC03> { [ Cyrillic_ie,	    Cyrillic_IE,    ae,              AE         ] };
    key <AC04> { [ Cyrillic_u,	    Cyrillic_U,	    eacute,          Eacute     ] };
    key <AC05> { [ Cyrillic_i,      Cyrillic_I     ] };

    // home row, right side

    key <AC06> { [ Cyrillic_de,	    Cyrillic_DE,    eth,             ETH        ] };
    key <AC07> { [ Cyrillic_ha,	    Cyrillic_HA,    dead_acute                  ] };
    key <AC08> { [ Cyrillic_te,	    Cyrillic_TE,    thorn,           THORN      ] };
    key <AC09> { [ Cyrillic_en,	    Cyrillic_EN,    ntilde,          Ntilde     ] };
    key <AC10> { [ Cyrillic_es,	    Cyrillic_ES,    ssharp                      ] };
    key <AC11> { [ minus,           underscore,     hyphen                      ], type[Group1] = "FOUR_LEVEL_ALPHABETIC" };
    key <BKSL> { [ backslash,       bar                                         ] };

    // lower row, left side
    key <AB01> { [ Cyrillic_yu,      Cyrillic_YU                  ] };
    key <AB02> { [ Cyrillic_ya, Cyrillic_YA] };
    key <AB03> { [ Cyrillic_shorti,   Cyrillic_SHORTI] };
    key <AB04> { [ Cyrillic_ka, Cyrillic_KA] };
    key <AB05> { [ Cyrillic_softsign, Cyrillic_SOFTSIGN] };

    //lower row, right side
    key <AB06> {   [ Cyrillic_be,       Cyrillic_BE         ]    };
    key <AB07> {   [ Cyrillic_em,       Cyrillic_EM         ]    };
    key <AB08> {   [ Cyrillic_zhe,      Cyrillic_ZHE        ]    };
    key <AB09> {   [ Cyrillic_ve,       Cyrillic_VE         ]    };
    key <AB10> {   [ Cyrillic_ze,       Cyrillic_ZE         ]    };
};
.
w
EOF
