#/usr/bin/ruby

arr = [3, -5, 5, 7, -3, 4, -1]

sum = max_sum = t = 0
idx = max_idx_start = max_idx_end = 0

arr.each_with_index {|e, i|
  t = sum + e
  if t > e
    sum = t
  else
    sum = e
    idx = i
  end

  if sum > max_sum
    max_sum = sum
    max_idx_start = idx
    max_idx_end = i
  end
}

p arr[max_idx_start .. max_idx_end]
puts max_sum
