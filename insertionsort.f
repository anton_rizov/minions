: cell-  1 cells - ;

: insert ( a-start a-end -- start )
    dup @ >r ( r: v )
    begin
	2dup <
    while
	r@  over cell- @   <
    while
	cell- dup @ over cell+ !
	\ dup cel- dup @ rot !
    repeat then
    r> swap ! ;

: sort ( array len -- )
    1 ?do
	dup i cells + insert
    loop drop ;

create test 7 , 3 , 0 , 2 , 9 , 1 , 6 , 8 , 4 , 5 ,

: main
    test 10 sort
    10 0 do test i cells + @ . loop cr ;

main bye