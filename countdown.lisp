(defun applicable-op-p (op a b)
  (or (not (eq op '/))
      (zerop b)))

(defun apply-op (op a b)
  (ecase op
    ('/ (/ a b))
    ('+ (+ a b))
    ('- (- a b))
    ('* (* a b))))

(defun solve-1 (target result expression numbers)
  (if (null numbers)
      (if (= target result)
          (print expression))
      (dolist (b numbers)
        (let ((next (remove b numbers :count 1)))
          (dolist (op '(+ - / *))
            (if (applicable-op-p op result b)
                (solve-1 target
                         (apply-op op result b)
                         (list op expression b)
                         next)))))))

(defun solve (target numbers)
  (solve-1 target (car numbers) (car numbers) (cdr numbers)))

