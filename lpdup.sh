#!/bin/zsh
file=$1
first=$2
last=$3

LP(){
    local range=$1
    local inputslot="-o InputSlot=Tray3"
    #inputslot="-o InputSlot=Tray2"

    sleep 1m
    
    lp -o page-ranges=$range -o media=A4 -o PageSize=A4 $inputslot -o Binding=LeftBinding $file
}

integer i
for ((i = $first; i < $last; i += 2)); do
    LP $i-$((i + 1))
done

if ((i == $last)); then
    LP $i
fi
