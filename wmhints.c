//-*- compile-command: "cc -o wmhints wmhints.c -lX11" -*-
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void die(const char *format, ...)
{
        va_list ap;
        va_start(ap, format);
        vfprintf(stderr, format, ap);
        va_end(ap);

        exit(-1);
}

#define FLAGS FLAG(InputHint)                   \
        FLAG(StateHint)                         \
        FLAG(IconPixmapHint)                    \
        FLAG(IconWindowHint)                    \
        FLAG(IconPositionHint)                  \
        FLAG(IconMaskHint)                      \
        FLAG(WindowGroupHint)                   \
        FLAG(XUrgencyHint)                      \
        FLAG(AllHints)

long name_to_value(const char *flag)
{
#define FLAG(name) if (strcmp(#name, flag) == 0) return name;

        FLAGS

        die("Unknown flag: %s\n", flag);
}

void show_flags(long value)
{
#define FLAG(name)                                                      \
        printf("%-20s - %s\n", #name, ((value & name) == name) ? "On" : "Off");

        FLAGS
}

int main(int argc, char **argv)
{
        Display    *display;
        Window      window;
        XWMHints   *hints;
        int         i;
        const char *name;
        long        value;

        if (argc < 2) {
                die("usage: %s <id> flags\n", *argv);
        }

        if (sscanf(argv[1], "0x%lx", &window) != 1) {
                die("id must be 0x%%lx\n");
        }

        display = XOpenDisplay("");
        if (!display) {
                die("Cannot open display\n");
        }

        hints = XGetWMHints(display, window);
        if (!hints) {
                hints = XAllocWMHints();
                if (!hints) {
                        die("Cannot allocate hints.\n");
                }
                hints->flags = InputHint;
        }

        if (argc == 2) {
                show_flags(hints->flags);
        }
        else {

                for (i = 2; i < argc; ++i) {
                        name = argv[i];
                        value = name_to_value(name + 1);
                        switch (name[0]) {
                        case '+':
                                hints->flags |= value;
                                break;
                        case '@':
                                hints->flags ^= value;
                                break;
                        case '-':
                                hints->flags &= ~value;
                                break;
                        default:
                                die("Don't know what to do with: %s\n", name);
                        }
                }

                XSetWMHints(display, window, hints);
        }

        XFree(hints);

        XFlush(display);
}
