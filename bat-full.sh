#/bin/sh

bat=/sys/class/power_supply/BAT0
full=`cat $bat/charge_full_design`
current=0
while((current < full)); do
    current=`cat $bat/charge_now`
    sleep 60
done
beep