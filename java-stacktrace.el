;;; FIXME: next-exception shouldn't move after last exception
(defgroup java-stacktrace nil
  "Cumstomization options for java stacktrace mode")

(defcustom java-stacktrace-resolve-partial-path-function nil
  "Function used to resolve PARTIAL-PATH to actual path"
  :type 'function
  :group 'java-stacktrace)

(defcustom java-stacktrace-frame-action-function
   'java-stacktrace-view-frame-idea
   "Function of two argument FILE and LINE"
   :type 'function
   :group 'java-stacktrace)

(defcustom java-stacktrace-project-package-prefix
  nil
  "Package prefix for project: e.g. \"com.yourcompany.yourproject\"."
  :type 'string
  :group 'java-stacktrace)

(defvar java-stacktrace-frame-regexp "^[ \t]+at \\([a-zA-Z0-9.]*\\)\\(?:\\$?[a-zA-Z0-9]*\\)\\.\\([^(]+\\)")

(defvar java-stacktrace-continuation-regexp "Caused by: ")
(defvar java-stacktrace-trailer-frame-regexp "^\\s-+\\.\\.\\. [0-9]+")

;;; todo: no reason to use overlay.  Two markers could do just fine
(make-variable-buffer-local
 (defvar java-stacktrace-overlay nil))

(define-button-type 'java-stacktrace-xref
  'follow-link t
  'action #'java-stacktrace-button-action)

(defun java-stacktrace-button-action (button)
  (funcall java-stacktrace-frame-action-function
           (button-get button 'java-stacktrace-file)
           (button-get button 'java-stacktrace-line)))

(defun java-stacktrace-project-class-p (class)
  (and (stringp class)
       java-stacktrace-project-package-prefix
       (string-prefix-p java-stacktrace-project-package-prefix class)))

(defun java-stacktrace-xref-1 ()
  ;; (assert (looking-at java-stacktrace-frame-regexp))
  (when (and java-stacktrace-resolve-partial-path-function
             (fboundp java-stacktrace-resolve-partial-path-function))
    (let ((class (match-string 1))
          (start (match-beginning 1))
          file line)
      (when (java-stacktrace-project-class-p class)
        (when (re-search-forward "(\\([^:]+\\):\\([0-9]+\\)"
                                 (line-end-position)
                                 t)
          (setq file (match-string 1)
                line (match-string 2)))
        (let ((path (funcall java-stacktrace-resolve-partial-path-function
                             (with-temp-buffer
                               (insert class)
                               (goto-char (point-min))
                               (while (search-forward "." nil t)
                                 (replace-match "/"))
                               (cond (file (delete-region (point)
                                                          (point-max))
                                           (insert file))
                                     (t (goto-char (point-max))
                                        (insert ".java"))) 
                               (buffer-string)))))
          (when path
            (make-button start
                         (progn
                           (search-forward ")")
                           (point))
                         'type 'java-stacktrace-xref
                         'java-stacktrace-file path
                         'java-stacktrace-line line)))))))

(defun java-stacktrace-xref ()
  (save-excursion
    (goto-char (java-stacktrace-start))
    (let ((inhibit-read-only t))
      (while (re-search-forward java-stacktrace-frame-regexp
                                (java-stacktrace-end)
                                t)
        (java-stacktrace-xref-1)
        (forward-line)))))

(defun java-stacktrace-hide-uninteresting-frames ()
  (interactive)
  (save-excursion
    (let ((inhibit-read-only t))
      (goto-char (java-stacktrace-start))
      (when (re-search-forward java-stacktrace-frame-regexp
                                 (java-stacktrace-end)
                                 t)
          (let* ((pos (line-beginning-position))
                 (button (next-button pos)))
            (while button
              (goto-char (button-end button))
              (put-text-property pos (line-beginning-position) 'invisible t)
              (forward-line)
              (setq pos (point))
              (setq button (next-button pos)))
            (put-text-property pos (java-stacktrace-end) 'invisible t))))))

(defun java-stacktrace-show-all-frames ()
  (interactive)
  (let ((inhibit-read-only t))
    (remove-list-of-text-properties (java-stacktrace-start)
                                    (java-stacktrace-end)
                                    '(invisible))))

(defmacro java-stacktrace-save-line-position (&rest body)
  (declare (indent 0))
  (let ((oldpoint (gensym "point"))
        (oldline (gensym "line")))
    `(let ((,oldpoint (point))
           (,oldline (line-beginning-position)))
       (beginning-of-line)
       (unwind-protect
           (progn ,@body)
         (when (= (line-beginning-position) ,oldline)
           (goto-char ,oldpoint))))))

(defun java-stacktrace-forward-frame (&optional arg)
  (setq arg (or arg 1))
  (java-stacktrace-save-line-position
    (if (< arg 0)
        (re-search-backward java-stacktrace-frame-regexp nil nil (- arg))
      (re-search-forward java-stacktrace-frame-regexp nil nil arg))
    (beginning-of-line)))

(defun java-stacktrace-activate (beg end)
  (if java-stacktrace-overlay
      (move-overlay java-stacktrace-overlay beg end)
    (setq java-stacktrace-overlay (make-overlay beg end)))
  (java-stacktrace-xref))

(defun java-stacktrace-active-p ()
  java-stacktrace-overlay)

(defun java-stacktrace-start ()
  (overlay-start java-stacktrace-overlay))

(defun java-stacktrace-end ()
  (overlay-end java-stacktrace-overlay))

(defun java-stacktrace-next-exception (&optional arg)
  (interactive "p")
  (setq arg (or arg 1))
  (java-stacktrace-forward-stacktrace arg t)
  (java-stacktrace-beginning-of-stacktrace-proper)
  (let ((beg (point))
        (end (save-excursion
               (java-stacktrace-end-of-stacktrace-proper)
               (point))))
    (java-stacktrace-activate beg end)
    (java-stacktrace-flash-region beg end)))

(defun java-stacktrace-previous-exception (&optional arg)
  (interactive "p")
  (java-stacktrace-next-exception (- (or arg 1))))

(defun java-stacktrace-forward-stacktrace (&optional arg proper)
  (interactive "p")
  (or arg (setq arg 1))
  (let (dir fun)
    (if (< arg 0)
        (setq dir -1
              arg (- arg)
              fun (if proper
                      'java-stacktrace-beginning-of-stacktrace-proper
                    'java-stacktrace-beginning-of-stacktrace))
      (setq dir 1
            fun (if proper
                    'java-stacktrace-end-of-stacktrace-proper
                  'java-stacktrace-end-of-stacktrace)))
    (while (> arg 0)
      (funcall fun)
      (java-stacktrace-forward-frame dir)
      (setq arg (1- arg)))))

(defun java-stacktrace-end-of-stacktrace-proper ()
  (while (progn
           (java-stacktrace-end-of-stacktrace)
           (looking-at java-stacktrace-continuation-regexp))))

(defun java-stacktrace-end-of-stacktrace ()
  (while (progn
           (forward-line)
           (looking-at java-stacktrace-frame-regexp)))
  (when (looking-at java-stacktrace-trailer-frame-regexp)
    (forward-line)))

(defun java-stacktrace-beginning-of-stacktrace-proper ()
  (while (progn
           (java-stacktrace-beginning-of-stacktrace)
           (looking-at java-stacktrace-continuation-regexp))
    (forward-line -1)))

(defun java-stacktrace-beginning-of-stacktrace ()
  (when (looking-at java-stacktrace-trailer-frame-regexp)
    (forward-line -1))
  (while (looking-at java-stacktrace-frame-regexp)
    (forward-line -1)))

(defun java-stacktrace-narrow ()
  (interactive)
  (when (java-stacktrace-active-p)
    (narrow-to-region (java-stacktrace-start)
                      (java-stacktrace-end))))

(defun java-stacktrace-mark-stacktrace (&optional arg)
  (interactive "P")
  (when (java-stacktrace-active-p)
    (set-mark (java-stacktrace-start))
    (goto-char (java-stacktrace-end))))

(defun java-stacktrace-flash-region (start end &optional timeout)
  (let ((overlay (make-overlay start end)))
    (overlay-put overlay 'face 'secondary-selection)
    (run-with-timer (or timeout 0.2) nil 'delete-overlay overlay)))

(defun java-stacktrace-view-frame-idea (file line)
  (call-process-shell-command "idea.sh" nil nil nil "--line" line file))

(defun java-stacktrace-view-frame-emacs (file line)
  (find-file file)
  (goto-line line))

(defun java-stacktrace-copy-as-kill ()
  (interactive)
  (kill-new (buffer-substring (java-stacktrace-start)
                              (java-stacktrace-end))))

(defvar  java-stacktrace-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map button-buffer-map)
    (define-key map "[" 'java-stacktrace-next-exception)
    (define-key map "]" 'java-stacktrace-previous-exception)
    (define-key map "{" 'forward-button)
    (define-key map "}" 'backward-button)
    (define-key map "`" 'forward-button)
    (define-key map "@" 'java-stacktrace-narrow)
    (define-key map "*" 'java-stacktrace-mark-stacktrace)
    (define-key map "#" 'java-stacktrace-copy-as-kill)
    map))

(define-minor-mode java-stacktrace-mode
  "Minor mode for working with java stacktraces."
  :initial-value nil
  :lighter " StackTrace"
  :keymap java-stacktrace-mode-map)

(provide 'java-stacktrace)
