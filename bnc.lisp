(defstruct try guess bulls cows)

(defun number-digits (num)
  "Returns list of digits in number"
  (loop for digit across (format nil "~a" num)
        collect (digit-char-p digit)))

(defun number-digits-b (num)
  (do ((result '()))
      ((zerop num) (nreverse result))
    (multiple-value-bind (q r) (floor num 10)
      (push r result)
      (setq num q))))

(defun number-digits-bb (num)
  "Same as number-digits-b but uses multiple-value-setq"
  (do ((result '()) rem)
      ((zerop num) (nreverse result))
    (multiple-value-setq (num rem) (floor num 10))
    (push rem)))

(defun number-digits-c (num)
  (loop with rem
        until (zerop num) do
        (multiple-value-setq (num rem) (floor num 10))
        collect rem))

(defun next-number (num)
  (do ((l num (cdr l)))
      ((null l) (error "number overflow"))
    (if (> (incf (car l)) 9)
        (setf (car l) 0)
      (return-from next-number num))))

(defun next-number2 (num)
  (let ((carry 1))
    (mapcar #'(lambda (d)
                (multiple-value-bind (q r)
                    (floor (+ d carry) 10)
                  (setq carry q)
                  r))
            num)))

(defun next-number3 (num)
  (let ((carry 1) result)
    (dolist (d num)
      (let ((x (+ d carry)))
        (cond ((= x 10) (push 0 result))
              (t (push x result) 
                 (setq carry 0)))))
    (nreverse result)))



(defun compute-score (a b)
  (let ((bulls 0) (cows 0))
    (mapc #'(lambda (i j)
              (cond ((= i j) (incf bulls))
                    ((member i b) (incf cows))))
          a b)
    (values bulls cows)))

(defun compute-score2 (a b)
  (loop for i in a
        for j in b
        if (= i j) count 1 into bulls
        else if (member i b) count 1 into cows
        finally (return (values bulls cows))))

(defun same-score-p (guess try)
  (multiple-value-bind (b c)
      (compute-score guess (try-guess try))
    (and (= b (try-bulls try))
         (= c (try-cows try)))))

(defun no-duplicate-digits-p (num)
  (equal num (remove-duplicates num)))

(defun valid-guess-p (guess)
  (no-duplicate-digits-p guess))

(defun next-guess2 (num tries)
  (do ((guess (next-number num) (next-number guess)))
      ((and (valid-guess-p guess) 
            (every #'(lambda (try) (same-score-p guess try)) tries)) 
       guess)))

(defun next-guess (num tries)
  (loop for n = (next-number num) then (next-number n)
        until (and (valid-guess-p n)
                   (every #'(lambda (try) (same-score-p n try)) tries))
        finally (return n)))

(defun mktry (guess bulls cows)
  (make-try :guess (copy-list guess) :bulls bulls :cows cows))

(defun driver ()
  (format t "~&Enter number:") 
  (loop with secret = (reverse (number-digits (read)))
        with tries = (list)
        for guess = (list 4 3 2 1) then (next-guess guess tries) do
        (multiple-value-bind (bulls cows)
            (compute-score guess secret)
          (format t "~&~{~a~} ~a ~a" (reverse guess) bulls cows)
          (if (= bulls 4) (return))
          (push (mktry guess bulls cows) tries))))
     
(defun driver-ii ()
  (let ((tries (list))
        (guess (list 4 3 2 1))
        bulls cows)
    (loop
     (format t "~&~{~A~} #:" (reverse guess))
     (setq bulls (read))
     (when (= bulls 4) (return))
     (setq cows (read))
     (push (mktry guess bulls cows) tries)
     (setq guess (next-guess guess tries)))))
    

       
                          





