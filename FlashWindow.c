#define WINVER 0x0501
#define _WIN32_WINNT 0x0501

#include <stdio.h>
#include <windows.h>

#define NO_ARGS(argc) ((argc) == 1)

int main(int argc, char **argv)
{
	HWND handle = NO_ARGS(argc) ? GetConsoleWindow() : FindWindow(NULL, argv[1]);

    FLASHWINFO fwi;
    fwi.cbSize = sizeof(FLASHWINFO);
    fwi.hwnd = handle;
    fwi.dwTimeout = 0;
    fwi.dwFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
	FlashWindowEx(&fwi);
}