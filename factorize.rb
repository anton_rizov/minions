
multiple = 1211

factors = []
factor = 2
while multiple != 1 do
  while multiple % factor == 0 do
    multiple = multiple / factor
    factors << factor
  end
  factor = factor + 1
end

puts factors
