#!/bin/sh

install () {
    ln -si $(readlink -f $1) $2
}

install_hidden_at_home () {
    install $1 ~/.$1
}


install_hidden_at_home Xdefaults
install Xdefaults ~/.Xresources

install_hidden_at_home tmux.conf

