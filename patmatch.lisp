;;; Peter Norvig's pat-match PAIP p178 with minor deviations.

(defmacro define-constant (name value &optional doc)
  `(defconstant ,name (if (boundp ',name) (symbol-value ',name) ,value)
     ,@(when doc (list doc))))

(defconstant fail nil)
(define-constant no-bindings '((t . t)))

(defun varp (x)
  (and (symbolp x)
       (< 0 (length (symbol-name x)))
       (char= (char (symbol-name x) 0) #\?)))

(defmacro defalias (newname old)
  `(setf (symbol-function ',newname) (symbol-function ',old)))

(defalias get-binding assoc)
(defalias binding-var car)
(defalias binding-val cdr)
(defalias make-binding cons)

;; (defun get-binding (var bindings)
;;   (assoc var bindings))

;; (defun binding-var (binding)
;;   (car binding))

;; (defun binding-val (binding)
;;   (cdr binding))

;; (defun make-binding (var val)
;;   (cons val val))

(defun lookup (var bindings)
  (binding-val (get-binding var bindings)))

(defun extend-bindings (var val bindings)
  (cons (make-binding var val)
	(effective-bindings bindings)))

(defun effective-bindings (bindings)
  (if (eq bindings no-bindings)
      nil
      bindings))

(defun match-var (var input bindings)
  (let ((binding (get-binding var bindings)))
    (cond ((not binding) (extend-bindings var input bindings))
	  ((equal input (binding-val binding)) bindings)
	  (t fail))))

(defun pat-match (pattern input &optional (bindings no-bindings))
  (cond ((eq bindings fail) fail)
	((varp pattern)
	 (match-var pattern input bindings))
	((eql pattern input) bindings)
	((segment-pattern-p pattern)
	 (segment-matcher pattern input bindings))
	((single-pattern-p pattern)
	 (single-matcher pattern input bindings))
	((and (consp pattern) (consp input))
	 (pat-match (rest pattern)
		    (rest input)
		    (pat-match (first pattern)
			       (first input)
			       bindings)))
	(t fail)))

(defun segment-pattern-p (pattern)
  (and (consp pattern)
       (consp (first pattern))
       (segment-match-fn (first (first pattern)))))

(defun single-pattern-p (pattern)
  (and (consp pattern)
       (single-match-fn (first pattern))))

(defun segment-match-fn (x)
  (when (symbolp x) (get x 'segment-match)))

(defun single-match-fn (x)
  (when (symbolp x) (get x 'single-match)))

(defun segment-matcher (pattern input bindings)
  (funcall (segment-match-fn (first (first pattern)))
	   pattern input bindings))

(defun single-matcher (pattern input bindings)
  (funcall (single-match-fn (first pattern))
	   (rest pattern) input bindings))

(defun match-is (var-and-pred input bindings)
  (let* ((var (first var-and-pred))
	 (pred (second var-and-pred))
	 (new-bindings (pat-match var input bindings)))
    (if (or (eq new-bindings fail)
	    (not (funcall pred input)))
	fail
	new-bindings)))

(defun match-and (patterns input bindings)
  (cond ((eq bindings fail) fail)
	((null patterns) bindings)
	(t (match-and (rest patterns) input
		      (pat-match (first patterns)
				 input
				 bindings)))))

(defun match-or (patterns input bindings)
  (if (null patterns)
      fail
      (let ((new-bindings (pat-match (first patterns) input bindings)))
	(if (eq new-bindings fail)
	    (match-or (rest patterns) input bindings)
	    new-bindings))))

;; (defun match-or (patterns input bindings)
;;   (loop for pat in patterns
;; 	and for b = (pat-match pat input bindings)
;; 	unless (eq b fail) (return-from match-or b))
;;   fail)

(defun match-not (patterns input bindings)
  (if (match-or patterns input bindings)
      fail
      bindings))

(defun segment-match (pattern input bindings &optional (start 0))
  (destructuring-bind ((_ var) . pat) pattern
    (declare (ignore _))
    (if (null pat)
	(match-var var input bindings)
	(let ((pos (first-match-pos (first pat) input start)))
	  (if (null pos)
	      fail
	      (let ((b2 (pat-match pat
				   (subseq input pos)
				   (match-var var
					      (subseq input 0 pos)
					      bindings))))
		(if (eq b2 fail)
		    (segment-match pattern input bindings (1+ pos))
		    b2)))))))

(defun first-match-pos (pat input start)
  (cond ((and (atom pat) (not (varp pat)))
	 (position pat input :start start :test #'equal))
	((< start (length input)) start)
	(t nil)))

(defun segment-match+ (pattern input bindings)
  (segment-match pattern input bindings 1))

(defun segment-match? (pattern input bindings)
  (destructuring-bind ((_ var) . pat) pattern
    (declare (ignore _))
    (or (pat-match (cons var pat) input bindings)
	(pat-match pat input bindings))))

(defun match-if (pattern input bindings)
  (destructuring-bind ((_ test) . pat) pattern
    (declare (ignore _))
    ;; progv t t is illegal
    (if (let ((b (effective-bindings bindings)))
	  (progv
	      (mapcar #'binding-var b)
	      (mapcar #'binding-val b)
	    (eval test)))
	(pat-match pat input bindings)
	fail)))

;; (dolist (name '(is or and not))
;;   (setf (get (intern (format nil "?~A" name)) 'single-match)
;; 	(intern (format nil "match-~A" name))))

(setf (get '?is  'single-match) 'match-is)
(setf (get '?or  'single-match) 'match-or)
(setf (get '?and 'single-match) 'match-and)
(setf (get '?not 'single-match) 'match-not)

(setf (get '?*  'segment-match) 'segment-match)
(setf (get '?+  'segment-match) 'segment-match+)
(setf (get '??  'segment-match) 'segment-match?)
(setf (get '?if 'segment-match) 'match-if)



(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

(defun symb (&rest args)
  (values (intern (apply #'mkstr args))))

(defun expand-var (var)
  (or (let* ((name (symbol-name var))
	     (length (1- (length name))))
	(when (< 1 length) 
	  (let ((ch (char name length)))
	    (when (find ch "*+?")
	      (list (symb #\? ch)
		    (symb (subseq name 0 length)))))))
      var))

(defun pat-match-abbrev (symbol expansion)
  (setf (getf symbol 'pat-match-abbrev)
	(expand-pat-match-abbrev expansion)))

(defun expand-pat-match-abbrev (pat)
  (cond ((and (symbolp pat) (get pat 'pat-match-abbrev)))
	((and (varp pat) (expand-var pat)))
	((atom pat) pat)
	(t (mapcar #'expand-pat-match-abbrev pat))))

#||
(pat-match '(?x ?op ?y is ?z (?if (eql (funcall ?op ?x ?y) ?z)))
	   '(3 + 4 is 7))

(pat-match '(a (?* ?x) (?* y) d) '(a b c d))

(pat-match '(a (?* ?x) d (?* y)) '(a b d c d))

(pat-match-abbrev '?x* '(?* ?x))
(pat-match-abbrev '?x+ '(?+ ?x))
||#
