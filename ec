#!/bin/zsh

TITLE="$*"
CONTENT="
     #+BEGIN_EXAMPLE
$(cat | sed 's/^/     /g')
     #+END_EXAMPLE
"

if [ -n $TITLE ]
then
  CONTENT="   - ${TITLE}\n${CONTENT}"
fi

CONTENT=$(echo "$CONTENT" | sed 's/"/\\"/g')

emacsclient -c -n \
  -e "(progn (org-capture-string \"$CONTENT\" \"C\") (delete-frame))"