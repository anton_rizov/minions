#!/usr/bin/perl

sub sol1 {
  my $hi = shift;
  my $sum = 0;
  for my $n (1 .. $hi) {
	$sum += $n if $n % 3 == 0 || $n % 5 == 0;
  }
  return $sum;
}

sub sum1_n {
  my $n = shift;
  return ($n * ($n + 1)) / 2;
}

sub sum {
  my ($n, $d) = @_;
  use integer;
  return sum1_n($n / $d) * $d;
}

sub sol2 {
  my $hi = shift;
  use integer;
  return sum($hi, 3) + sum($hi, 5) - sum($hi, 15);
}

print sol1(999), "\n";
print sol2(999), "\n";
