//-*- compile-command: "javac pad.java && /opt/java/bin/java pad"; -*-
import java.util.ArrayList;

import static java.lang.System.out;
import static java.util.Arrays.asList;

class pad{
  static void replaceAll(){
    System.out.println("{a}{aaa{a}}".replaceAll("\\{a\\}", "10"));
  }

  static void shortOrIntValue(){
///////////////////////////////////////////////////////////////////////////////////////
//   static void shortOrIntValue();						     //
//     Code:									     //
//        0: bipush        20							     //
//        2: invokestatic  #8                  // Method java/lang/Short.valueOf:(S) //
// Ljava/lang/Short;                                                                 //
//        5: astore_0      							     //
//        6: aload_0       							     //
//        7: invokevirtual #9                  // Method java/lang/Short.shortValue: //
// ()S                                                                               //
//       10: istore_1      							     //
//       11: return   								     //
///////////////////////////////////////////////////////////////////////////////////////

    Short val = 20;
    int i = val;
  }


  static void arrayInstanceOf(){
    Object x = new int[]{1,2};
    out.println(x instanceof Object[]); // false
    out.println(x instanceof int[]);	// true
  }

  static void split(){
    out.println("".split(",").length);  // 1
    out.println(",".split(",").length); // 0
  }

  static void outerLoopLabel(){
    int[] x = new int[]{1,2,3};
    int[] y = new int[]{1,2,3};

    outer:
    for(int i:x){
      out.println(i);
      for(int j:y)
	if(j==2) continue outer;
    }
  }

  static void sublist(){
    ArrayList<String> list = new ArrayList<>();
    list.add("a");
    list.add("2");
    // Exception in thread "main" java.lang.IndexOutOfBoundsException: toIndex = 10
    // 	at java.util.ArrayList.subListRangeCheck(ArrayList.java:922)
    // 	at java.util.ArrayList.subList(ArrayList.java:914)
    // 	at pad.sublist(pad.java:58)
    // 	at pad.main(pad.java:62)

    out.println(list.subList(0, 10));
  }

  static void dotInCharClass(String[] args){
    for(String arg:args)
      out.println(asList(arg.split("[/.]")));
  }

  static void finalVar(){
    final int x;
    x = 10;
    out.println(x);
  }

//   javac pad.java && java pad
// pad.java:80: error: unreported exception IOException; must be caught or declared to be thrown
//     one, two, three;
//     ^
// pad.java:80: error: unreported exception IOException; must be caught or declared to be thrown
//     one, two, three;
//          ^
// pad.java:80: error: unreported exception IOException; must be caught or declared to be thrown
//     one, two, three;
//               ^
//   enum x{
//     one, two, three;

//     x() throws java.io.IOException {
//       throw new java.io.IOException();
//     }
//   }

  // 1.7 still creates two StringBuilders
  // static void stringAppend(int);
  // Code:
  //    0: new           #26                 // class java/lang/StringBuilder
  //    3: dup
  //    4: invokespecial #27                 // Method java/lang/StringBuilder."<init>":()V
  //    7: astore_1
  //    8: aload_1
  //    9: new           #26                 // class java/lang/StringBuilder
  //   12: dup
  //   13: invokespecial #27                 // Method java/lang/StringBuilder."<init>":()V
  //   16: ldc           #28                 // String xY
  //   18: invokevirtual #29                 // Method java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
  //   21: iload_0
  //   22: invokevirtual #30                 // Method java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
  //   25: invokevirtual #31                 // Method java/lang/StringBuilder.toString:()Ljava/lang/String;
  //   28: invokevirtual #29                 // Method java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
  //   31: pop
  //   32: return
  static void stringAppend(int x){
    StringBuilder b = new StringBuilder();
    b.append("x" + "Y" + x);
  }

  interface A{ void m(); }

  /* creates new class
final class pad$1 implements pad$A {
  final java.lang.String val$s;

  pad$1(java.lang.String);
    Code:
       0: aload_0
       1: aload_1
       2: putfield      #1                  // Field val$s:Ljava/lang/String;
       5: aload_0
       6: invokespecial #2                  // Method java/lang/Object."<init>":()V
       9: return

  public void m();
    Code:
       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: aload_0
       4: getfield      #1                  // Field val$s:Ljava/lang/String;
       7: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      10: return
}


  static pad$A newA(java.lang.String);
    Code:
       0: new           #32                 // class pad$1
       3: dup
       4: aload_0
       5: invokespecial #33                 // Method pad$1."<init>":(Ljava/lang/String;)V
       8: areturn
  */
  static A newA(final String s){
    return new A(){
      @Override
      public void m(){
	out.println(s);
      }
    };
  }

  static void byteCache(){
    Byte a = 44;
    Byte b = 44;
    out.println("a == b : " + (a == b)); // true
  }

  static void backref(){
    out.println("x=y".replaceAll("([xy])","($1)"));
  }

  static void negateMinInt(){
    int i = Integer.MIN_VALUE;
    out.println(i == -i);
  }

  static void negatingHasNoEffect(){
    for(int j = Integer.MIN_VALUE; j < Integer.MAX_VALUE; j++){
      if (j == -j)
	out.println(j);
    }
  }

  static void bigDecimalConstructor() {
    System.out.println(0.1);
    System.out.println(String.valueOf(0.1));
    //as described in constructor's java doc these two are different
    System.out.println(new java.math.BigDecimal(0.1));
    System.out.println(new java.math.BigDecimal(String.valueOf(0.1)));
  }

  static void switchWithNull() {
// javac pad.java && java pad
// Exception in thread "main" java.lang.NullPointerException
// 	at pad.switchWithNull(pad.java:195)
// 	at pad.main(pad.java:208)
    for (String e : new String[]{null, "blah"})
      switch(e){
      case "blag": out.println("blah"); break;
      case "drun": out.println("drun"); break;
      default: out.println("default" + e);
      }
  }

  static void instanceOfArray() {
    Object x = new int[3];
    out.println((x instanceof Object[]));
  }

  static void boxingCache() {
    boolean cache = false;
    for (int i = Integer.MIN_VALUE; ; i++) {
      if (Integer.valueOf(i) == Integer.valueOf(i)) {
        if (!cache) {
          cache = true;
          out.printf("[%d", i);
        }
      }
      else if (cache) {
        out.printf(",%d]", i-1);
        cache = false;
      }
      if (i == Integer.MAX_VALUE)
        break;
    }
  }

  static void castingLongToInt() {
    out.println((int)Long.MAX_VALUE);//-1
    out.println((int)Long.MIN_VALUE);//0
  }

  static void f(double x, double y) {
    out.println(x/y);
  }

  static void typeConversion() {
    f(1,2);
    out.println(1/2);
  }

  static void ebcdic() {
    try {
      String s = new String("abc".getBytes("ASCII"), "IBM1047");
      out.println(s);
    } catch(Exception ignore){
    }
  }

  public static void main(String[] args) {
    long x = 60 * 60 * 24 * 1_000;
    long y = 60 * 60 * 24 * 1_000 * 1_000;
    System.out.println(y / x);
    ebcdic();
    //fool emacs: don't close *compilation* buffer
    System.exit(1);
  }
}
