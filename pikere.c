#include <stdio.h>
#include <stdlib.h>

int match(const char *regexp, const char *text)
{
	if (regexp[0] == '^')
		return matchhere(regexp + 1, text);

	do {
		if (matchhere(regexp, text))
			return 1;
	} while (*text++ != '\0');

	return 0;
}

int matchstar(char c, const char *regexp, const char *text);
int matchplus(char c, const char *regexp, const char *text);

int matchhere(const char *regexp, const char *text)
{
	if (regexp[0] == '\0')
		return 1;

	if (regexp[0] == '\\')
		return matchhere(regexp + 1, text);

	if (regexp[0] == '$' && regexp[1] == '\0')
		return *text == '\0';

	if (regexp[1] == '*')
		return matchstar(regexp[0], regexp + 2, text);

	if (regexp[1] == '+')
		return matchplus(regexp[0], regexp + 2, text);

	if (text != '\0' && (regexp[0] == '.' || regexp[0] == text[0]))
		return matchhere(regexp + 1, text + 1);

	return 0;
}

/* Greedy * */
int matchstar(char c, const char *regexp, const char *text)
{
	const char *t = text;

	while (*t != '\0' && (*t++ == c || c == '.'))
		;

	do {
		if (matchhere(regexp, t))
			return 1;
	} while (t-- > text);

	return 0;
}


#if (0)
/* Non greedy *? */
int matchstar(char c, const char *regexp, const char *text)
{
	do {
		if (matchhere(regexp, text))
			return 1;
	} while (*text != '\0' && (*text++ == c || c == '.'));
	return 0;
}
#endif

int matchplus(char c, const char *regexp, const char *text)
{
	return *text == c && matchstar(c, regexp, text + 1);
}

void t(const char *regexp, const char *text)
{
	printf("%d %20s =~ /%s/\n", match(regexp, text), text, regexp);
}

int main()
{
	t("a*","abc");
	t("^abc","babc");
	t("ab$", "abx");
	t("xy*z", "abxyyyzxb");
	t("y*", "x");
	t("y+", "x");
	t("y+", "xy");
	t("x$", "x$y");
	t("x\\$", "x$y");
	t("x\\*", "-x*-");
	t("\\.", "x");
	exit(0);
}
